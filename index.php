<!DOCTYPE html>
<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Nunito' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/default.css"/>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<title>/TS Online</title>
</head>
<body role="document" class="scroll-area" data-spy="scroll">
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" type="button">
				<span class="sr-only"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			</button> 
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li id="meHeader"><a class="page-scroll" href="#me">Me</a></li>
				<li id="techHeader"><a class="page-scroll" href="#tech">Tech</a></li>
				<li id="workHeader"><a class="page-scroll" href="#work">Work</a></li>
				<li id="portfolioHeader"><a class="page-scroll" href="#portfolio">Portfolio</a></li>
				<li id="resumeHeader"><a class="page-scroll" href="#resume">Resume</a></li>
			</ul>
		</div>
	</div>
</nav>
<div class="container-fluid">
	<div class="row">
		<div class="container-fluid theme-showcase me" role="main" id="me">
			<div class="container middle">
				<div class="jumbotron">
					<h1>/Taylor_Swann</h1>
					<h4 class="me">System Administrator</h4>
					<h4 class="me"><a href="mailto:taylor@taylorswann.com" class="me">taylor@taylorswann.com</a></h4>
					<h4 class="me">321 445 1091</h4>
					<img class="me" src="img/aws_cert.png"/>
					</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="gap" id="tech" style="margin-top:0px;"></div>
	<div class="row">
		<div class="container-fluid tech">
			<div class="container mobileReady">
				<h1 class="sectionTitle">/Technologies</h1>
				<div class="container techContent mobileReady">
					<?php
					$colors = array('#003258','#8e908f');
					$skills = array('RHEL/CentOS:70%','Windows:65%','OS X:30%','VMWare:60%','AWS:65%','Ansible:40%','PHP:65%','Python:50%','Bash:30%','PostgreSQL:55%','MySQL:30%','Apache:50%','Nginx:35%');
					shuffle($colors);
					for ($i=0;$i<sizeof($skills);$i++)
					{
						$items = explode(":",$skills[$i]);
						$skill = $items[0];
						$percent = $items[1];
						if ($i % 2)
							$color = $colors[0];
						else
							$color = $colors[1];
					?>
						<div class="skillbar clearfix" data-percent="<?php echo $percent;?>">
							<div class="skillbar-title" ><span style="background-color:<?php echo $color;?>"><?php echo $skill;?></span></div>
							<div class="skillbar-bar" style="background-color:<?php echo $color;?>"></div>
						</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="gap" id="work"></div>
	<div class="row">
		<div id="workMaster" class="container-fluid work">
			<div class="container mobileReady">
				<h1 class="sectionTitle">/Work</h1>
				<div class="container workContent mobileReady">
					<h1>Promo Only, Inc.</h1>
					<h3 style="margin-top:20px;">System Administrator, January 2011 - Present</h3>
					<p class="work">
						As the sole system administrator, primary responsibilities include configuration and maintenance of various servers around the country which consist of web, database, email, file, application, DHCP/DNS/VPN/FTP and domain controllers. Responsibilities also include network monitoring and security in the Orlando office and assisting in team problem solving when any issue is encountered. Secondary responsibilities include IT helpdesk support for all offices around the world, assisting our technical support staff with escalated customer issues, developing backend support systems, and integrating opensource solutions for employees.
					</p>
					<h3 style="margin-top:20px;">Junior Web Developer, May 2010 - January 2011</h3>
					<p class="work">
						Developed various systems used to support backend operations in the company, most notably being a website used to view usage statistics for our primary product and a distribution list management system used to distribute content to our subscribers. The primary technologies used were Apache, PHP, HTML/CSS and PostgreSQL.
					</p>
					<h4 style="margin-top:20px;">Accomplishments:</h4>
					<ul class="work">
						<li>Worked alongside software developers to build a custom CDN that serves multiple applications at a very low operating cost.</li>
						<li>Moved from a physical to virtual server environment and later a high availability VMWare cluster.</li>
						<li>Developed a custom scalable storage solution for our media database using windows/linux file servers and iSCSI storage array's.</li>
						<li>Created a custom 3-2-1 backup system used to protect 150 TB+ of  valuable company data and provide high availability for mission critical applications.</li>
						<li>Redesigned our Orlando office network infrastructure for increased performance and high availability.</li>
						<li>Implemented various open-source applications to drive business and assist our support/sales departments.</li>
						<li>Implemented Ansible Configuration Management for various server roles in our infrastructure.</li>
						<li>Tasked with the migration of specific applications to Amazon Web Services, for high availability and cost reduction.</li>
					</ul>
				</div>
				<div class="container workContent mobileReady">
					<h1>Harry Warren, Inc.</h1>
					<h3 style="margin-top:20px;">IT Helpdesk Technician, January 2009 - May 2010</h3>
					<p class="work">
						Responsible for troubleshooting IT related issues for employees within the company which include windows workstation configuration and maintenance, network connectivity, and printer related issues.  
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="gap" id="portfolio"></div>
	<div class="row"><!---->
		<div class="container-fluid portfolio">
			<div class="container">
				<h1 class="sectionTitle">/Portfolio</h1>
				<div class="container resumeContent">
					<div style="margin-left:auto;margin-right:auto;width:175px;">
						<a href="https://bitbucket.org/t_swann">
							<img class="resume" src="img/bbicon.png" alt="BitBucket Icon"/>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="gap" id="resume"></div>
	<div class="row"><!---->
		<div class="container-fluid resume">
			<div class="container">
				<h1 class="sectionTitle">/Resume</h1>
				<div class="container resumeContent">
					<div style="margin-left:auto;margin-right:auto;width:350px;">
						<a href="https://docs.google.com/document/d/13kARCOiL2LvVpVZOgBk2TKq0ACgUcAcfPk9q5InZnA8/edit?usp=sharing">
							<img class="resume" src="img/gicon.png" alt="Google Drive Icon"/>
						</a>
						<a href="https://docs.google.com/document/d/13kARCOiL2LvVpVZOgBk2TKq0ACgUcAcfPk9q5InZnA8/export?format=pdf">
							<img class="resume" src="img/pdficon.png" alt="PDF Icon"/>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="container-fluid">
			<div class="container" style="width:350px; margin-left:auto;margin-right:auto;">
				<p style="color:#fff;text-align:center;">Copyright&copy; <?php echo date('Y');?> taylorswann.com</p>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/default.js"></script>
</body>
</html>
