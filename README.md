# README #

Welcome to a behind the scenes look at taylorswann.com!  While I pride myself on my sysadmin/devops prowess, I love working on projects and finding creative solutions to the problems I run into.

Thanks for visiting!