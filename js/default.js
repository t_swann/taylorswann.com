//Resize work and tech sections for readability on mobile browsers
$(document).ready(function(){
    var $window = $(window);
    var $pane = $('#panel');

    function checkWidth(){
        var windowsize = $window.width();
        if (windowsize < 440){
            $('.mobileReady').addClass('container-fluid').removeClass('container');
            $('.container-fluid').css({"padding-right":"5px", "padding-left": "5px"});
        }else{
            $('.mobileReady').addClass('container').removeClass('container-fluid');
            $('.container-fluid').css({"padding-right":"15px", "padding-left": "15px"});
        }
    }

    checkWidth();
    $(window).resize(checkWidth);
});

//Trigger the skill bars to execute when the browser scrolls into the tech section
var fireSkills = 0;
$(window).scroll(function(){
    if (fireSkills == 0){
        //jQuery(document).ready(function(){
        if ($('#techHeader').hasClass('active')){
            jQuery('.skillbar').each(function(){
                jQuery(this).find('.skillbar-bar').animate({
                width:jQuery(this).attr('data-percent')
                },6000);
            });    
            fireSkills = 1;
        }
    }
});


$(document).ready(function(){
    $(".scroll-area").scrollspy({target: "#navbar"}) 
});
//jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});